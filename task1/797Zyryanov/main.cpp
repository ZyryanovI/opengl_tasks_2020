#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#define GLM_ENABLE_EXPERIMENTAL
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <chrono>
#include <experimental/filesystem>
#include <iomanip>
#include <iostream>
#include <vector>
#include <glm\fwd.hpp>
#include <glm\ext\vector_float3.hpp>
#include <glm\geometric.hpp>
#include <glm\gtx\transform.hpp>
#include <glm\ext.hpp>

#define M_PI 3.13

int countMax = 100;
int countActual = 0;
float delta = 1.0f;
float offsetX = 0.0f;
float offsetY = 0.0f;
float* vertexes = nullptr;

glm::mat4 matrix = glm::lookAt(glm::vec3{ 0.6f, 1.f, 1.f }, { 0.f, 0.f, 0.f }, { 0.f, 0.f, 1.f });

// common/camera
float* make_coords();

//������� ��������� ������ ��� ��������� ������� ����������
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE)
    {
        //���� ������ ������� ESCAPE, �� ��������� ����
        glfwSetWindowShouldClose(window, GL_TRUE);
    }

    if (key == GLFW_KEY_KP_ADD) {
        if (countMax == 0) {
            countMax = 20;
        }
        countMax += 10;
        vertexes = make_coords();
        glBufferData(GL_ARRAY_BUFFER, countActual * sizeof(float), vertexes, GL_STATIC_DRAW);
    }

    if (key == GLFW_KEY_KP_SUBTRACT) {
        countMax -= 10;
        if (countMax <= 0) {
            countMax = 10;
        }
        vertexes = make_coords();
        glBufferData(GL_ARRAY_BUFFER, countActual * sizeof(float), vertexes, GL_STATIC_DRAW);
    }

    if (key == GLFW_KEY_DOWN) {
        delta -= 0.01;
        vertexes = make_coords();
        glBufferData(GL_ARRAY_BUFFER, countActual * sizeof(float), vertexes, GL_STATIC_DRAW);
    }

    if (key == GLFW_KEY_UP) {
        delta += 0.01f;
        vertexes = make_coords();
        glBufferData(GL_ARRAY_BUFFER, countActual * sizeof(float), vertexes, GL_STATIC_DRAW);
    }

    if (key == GLFW_KEY_LEFT) {
        offsetX -= 0.01f;
        vertexes = make_coords();
        glBufferData(GL_ARRAY_BUFFER, countActual * sizeof(float), vertexes, GL_STATIC_DRAW);
    }

    if (key == GLFW_KEY_RIGHT) {
        offsetX += 0.01f;
        vertexes = make_coords();
        glBufferData(GL_ARRAY_BUFFER, countActual * sizeof(float), vertexes, GL_STATIC_DRAW);
    }

    if (key == GLFW_KEY_A) {
        const float alpha = 0.01;
        matrix *= glm::mat4(glm::vec4{ cos(alpha), sin(alpha), 0, 0 }, { -sin(alpha), cos(alpha), 0, 0 }, { 0, 0, 1, 0 }, glm::vec4{ 0, 0, 0, 1 });
    }

    if (key == GLFW_KEY_D) {
        const float alpha = -0.01;
        matrix *= glm::mat4(glm::vec4{ cos(alpha), sin(alpha), 0, 0 }, { -sin(alpha), cos(alpha), 0, 0 }, { 0, 0, 1, 0 }, glm::vec4{ 0, 0, 0, 1 });
    }

    if (key == GLFW_KEY_W) {
        const float beta = 0.01;
        matrix *= glm::mat4(glm::vec4{ 1, 0, 0, 0 }, { 0, cos(beta), sin(beta), 0 }, { 0, -sin(beta), cos(beta), 0 }, glm::vec4{ 0, 0, 0, 1 });
    }

    if (key == GLFW_KEY_S) {
        const float beta = -0.01;
        matrix *= glm::mat4(glm::vec4{ 1, 0, 0, 0 }, { 0, cos(beta), sin(beta), 0 }, { 0, -sin(beta), cos(beta), 0 }, glm::vec4{ 0, 0, 0, 1 });
    }


}

void add(float* answer, std::vector<std::vector<glm::vec3>>& points, int i, int j) {
    answer[countActual++] = points[i][j].x;
    answer[countActual++] = points[i][j].y;
    answer[countActual++] = points[i][j].z;
}

void add(float* answer, glm::vec3 a) {
    answer[countActual++] = a.x;
    answer[countActual++] = a.y;
    answer[countActual++] = a.z;
}

float* make_coords() {


    const float aa = 1;
    const float bb = 2;
    const float cc = 1;

    const float u_max = M_PI;

    const float v_max = M_PI;
    
    std::vector<std::vector<glm::vec3>> points;
    int count_v = countMax;
    int count_u = count_v;
    countActual = 0;

    //x= aa � sin u cos v, y = bb � sin u sin v, z = cc � cos u,

    float deltaU = 2 * u_max / (count_u);
    float deltaV = 2 * v_max / (count_v);


    for (float u = -u_max, k = 0; k <= count_u; u = -u_max + 2 * u_max * k / (count_u), k++) {
        std::vector<glm::vec3> pointTemp;
        for (float v = -v_max, k1 = 0; k1 <= count_v; v = -v_max + 2 * v_max * k1 / (count_v), k1++) {
            float x = aa * sin(u) * cos(v) * delta + offsetX;
            float y = bb * sin(u) * sin(v) * delta;
            float z = cc * cos(u) * delta;

           
            pointTemp.push_back({ x, y, z });

        }
        points.push_back(pointTemp);
    }

    float* answer = new float[(points.size() * points[0].size() + 2) * 36];

    for (int i = points.size(); i > 0; i--) {
        for (int j = 1; j <= points[0].size(); j++) {
            auto cros = cross(points[i - 1][j - 1] - points[i % points.size()][j - 1], points[i - 1][j % points[0].size()] - points[i % points.size()][j - 1]);
            float len = length(cros);
            cros /= len;


            if (len < 0.0000001) {
                continue;
            }

            add(answer, points, i - 1, j - 1);
            add(answer, cros);
            add(answer, points, i % points.size(), j - 1);
            add(answer, cros);
            add(answer, points, i - 1, j % points[0].size());
            add(answer, cros);

            add(answer, points, i % points.size(), j - 1);
            add(answer, cros);
            add(answer, points, i - 1, j % points[0].size());
            add(answer, cros);
            add(answer, points, i % points.size(), j % points[0].size());
            add(answer, cros);

        }

    }
    


    std::cout << "answer_returned" << std::endl;
    std::cout << countActual << std::endl;
    return answer;
}

int main()
{
    // �������, �����������, ��� ���� � ����� Data ��������� �� ���� ���.
    // �� ������ �� ���� ���� �� �����.

    // This sample shows that Data files are overwritten on each run using IDE.
    using namespace std::experimental;
    std::cout << "Hello world!\n";

    std::cout << "Working dir: " << filesystem::current_path() << std::endl;

    filesystem::path checkPath("123IvanovData2/simple.vert");
    if (filesystem::exists(checkPath)) {
        std::cout << "simple.vert found" << std::endl;
        auto timePoint = filesystem::last_write_time(checkPath);
        auto time = std::chrono::system_clock::to_time_t(timePoint);
        std::cout << "Time of last write: " << std::put_time(std::localtime(&time), "%F %T") << std::endl;
    }
    else {
        std::cerr << "simple.vert not found" << std::endl;
    }
    

    //�������������� ���������� GLFW
    if (!glfwInit())
    {
        std::cerr << "ERROR: could not start GLFW3\n";
        exit(1);
    }

    //������������� ��������� �������� ������������ ���������
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    //������� ����������� �������� (����)
    GLFWwindow* window = glfwCreateWindow(800, 600, "MIPT OpenGL demos", nullptr, nullptr);
    if (!window)
    {
        std::cerr << "ERROR: could not open window with GLFW3\n";
        glfwTerminate();
        exit(1);
    }

    //������ ���� �������� �������
    glfwMakeContextCurrent(window);

    //������������� ������� ��������� ������ ��� ��������� ������� ����������
    glfwSetKeyCallback(window, keyCallback);

    //�������������� ���������� GLEW
    glewExperimental = GL_TRUE;
    glewInit();

    //=========================================================

    //���������� ������ ������������

    vertexes = make_coords();


    //������� ����� VertexBufferObject ��� �������� ��������� �� ����������
    GLuint vbo;
    glGenBuffers(1, &vbo);

    //������ ���� ����� �������
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    //�������� ���������� ������� � ����� �� ����������
    glBufferData(GL_ARRAY_BUFFER, countActual * sizeof(float), vertexes, GL_STATIC_DRAW);

    //=========================================================

    //������� ������ VertexArrayObject ��� �������� �������� ������������� ������
    GLuint vao;
    glGenVertexArrays(1, &vao);

    //������ ���� ������ �������
    glBindVertexArray(vao);

    //������ ����� � ������������ �������
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    //�������� 0� ��������� �������
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    //������������� ���������:
    //0� �������,
    //3 ���������� ���� GL_FLOAT,
    //�� ����� �������������,
    //0 - �������� ����������� � ������� �������,
    //0 - ����� �� ������
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GL_FLOAT), reinterpret_cast<void*>(0));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GL_FLOAT), reinterpret_cast<void*>(3 * sizeof(GL_FLOAT)));

    glBindVertexArray(0);

    //=========================================================

    //��������� ������
    const char* vertexShaderText =
        "#version 450\n"

        "layout(location = 0) in vec3 vertexPosition;\n"
        "layout(location = 1) in vec3 normal;"
        "layout(location = 2) uniform mat4 viewMatrix;\n"

        "out vec3 color;"

        "void main()\n"
        "{\n"
        "   gl_Position = viewMatrix * vec4(vertexPosition, 1.0);\n"
        "color = 0.5 * normal + 0.5; // vertexPosition;\n"
        "}\n";

    //������� ��������� ������
    GLuint vs = glCreateShader(GL_VERTEX_SHADER);

    //�������� � ��������� ������ ����� �������
    glShaderSource(vs, 1, &vertexShaderText, nullptr);

    //����������� ������
    glCompileShader(vs);

    //��������� ������ ����������
    int status = -1;
    glGetShaderiv(vs, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint errorLength;
        glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetShaderInfoLog(vs, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to compile the shader:\n" << errorMessage.data() << std::endl;

        exit(1);
    }

    //=========================================================

    //����������� ������
    const char* fragmentShaderText =
        "#version 330\n"

        "out vec4 fragColor;\n"

        "in vec3 color;\n"

        "void main()\n"
        "{\n"
        "    fragColor = vec4(color, 1.0);\n"
        "}\n";

    //������� ��������� ������
    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);

    //�������� � ��������� ������ ����� �������
    glShaderSource(fs, 1, &fragmentShaderText, nullptr);

    //����������� ������
    glCompileShader(fs);

    //��������� ������ ����������
    status = -1;
    glGetShaderiv(fs, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint errorLength;
        glGetShaderiv(fs, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetShaderInfoLog(fs, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to compile the shader:\n" << errorMessage.data() << std::endl;

        exit(1);
    }

    //=========================================================

    //������� ��������� ���������
    GLuint program = glCreateProgram();

    //����������� ��������� �������
    glAttachShader(program, vs);
    glAttachShader(program, fs);

    //������� ���������
    glLinkProgram(program);

    //��������� ������ ��������
    status = -1;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint errorLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetProgramInfoLog(program, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to link the program:\n" << errorMessage.data() << std::endl;

        exit(1);
    }

    //=========================================================



    //���� ���������� (���� ���� �� �������)
    while (!glfwWindowShouldClose(window))
    {

        glUniformMatrix4fv(2, 1, GL_FALSE, glm::value_ptr(matrix));

        //    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            //��������� ������� ����� (����� ���������� ������� ��������� ������ ��� ��������� ������� ����������)
        glfwPollEvents();

        //�������� ������� ������ (����)
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);

        //������������� ���� ������ �� ���� ����� (����)
        glViewport(0, 0, width, height);

        //������� ���� ������ (����� ����� � ����� �������)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //���������� ��������� ���������
        glUseProgram(program);

        //���������� VertexArrayObject � ���������� ������������� ������
        glBindVertexArray(vao);

        //������ ������������� ������ (������� �� �������������, ����� 0, ���������� ������ 3)
        glDrawArrays(GL_TRIANGLES, 0, countActual / 3);

        glfwSwapBuffers(window); //����������� �������� � ������ ������
    }

    //������� ��������� ������� OpenGL
    glDeleteProgram(program);
    glDeleteShader(vs);
    glDeleteShader(fs);
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);

    glfwTerminate();

    return 0;
}